public class AddNIntegers {

    public static void main(String[] args) {
        int sum = 0;
		for(int i =0; i<args.length; i++){
			System.out.println((i+1)+"th is " + args[i]);
			sum = sum + Integer.parseInt(args[i]);
		}
        System.out.println("The sum is: " + sum);
    }
}