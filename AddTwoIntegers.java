public class AddTwoIntegers {

    public static void main(String[] args) {
        
        String first = args[0];
        String second = args[1];

        int sum = Integer.parseInt(first) + Integer.parseInt(second);

        System.out.println("The sum is: " + sum);
    }
}